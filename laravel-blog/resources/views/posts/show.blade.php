@extends('layouts.app')

@section('tabName')
	{{$post->title}}
@endsection

@section('content')
	<div class = "card col-6 mx-auto">
		<div class = 'card-body'>
			<h2 class = 'card-title'>{{$post->title}}</h2>
			<p class = "card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class = 'card-subtitle text-muted mb-3'>Created at: {{$post->created_at}}</p>
			<h4>Content:</h4>
			<p class = "card-text">{{$post->body}}</p>

			@if(Auth::id() != $post->user_id)
				<form class = "d-inline" method = "POST" action="/posts/{{$post->id}}/like">
					@method('PUT')
					@csrf
					@if($post->likes->contains('user_id', Auth::id()))
						<button class = "btn btn-danger">Unlike</button>
					@else
						<button class = "btn btn-success">Like</button>
					@endif
				</form>

			@endif
			<br/>
			<a href="/posts" class = "btn btn-info mt-2">View all posts</a>
		</div>		
	</div>

@endsection	