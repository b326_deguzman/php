@extends('layouts.app')

@section('content')
@if(session('message'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  {{session('message')}}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

	<form class = "col-3 bg-secondary p-5 mx-auto" method = "POST" action = '/posts/{{$posts->id}}'>
		<!-- CSRF stands for Cross-Site Request Forgery. It is form of attact where malicious users may send malicious request while pretending to be authorize user. Laravel uses token to detect if form input request have not been tampered with. -->
        
		@csrf
		<div class = "form-group">
			<label for = 'title'>Title:</label>
			<input type="text" name="title" class = "form-control" id="title" value='{{$posts->title}}' />
		</div>

		<div class = "form-group">
			<label for = "body">Content:</label>
			<textarea class = "form-control" id = "body" name = "body" rows =3>{{$posts->body}}</textarea>
		</div>

		<div class = 'mt-2 text-center'>
			<button class= "btn btn-primary">Update Post</button>
		</div>
	</form>
@endsection